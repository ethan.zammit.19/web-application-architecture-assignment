<?php
session_start();

require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';


if (isset($_SESSION['id'])) {
    if ($_SESSION['right'] == 1) {
        header("Location: admin.php");
        exit();
    } else {
        require 'favs.php';
        exit();
    }
    echo "<a href='includes/logout.inc.php'>Logout</a>";
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {

    //Make sure that the php script was accessed from the button
    if (isset($_POST['login'])) {
        require_once 'includes/dbc.inc.php';

        $emailphone = trim($_POST['emailphone']);
        $password = trim($_POST['pass']);

        $InternErr = $emptyErr = $emailPassErr = $oldEmail = "";


        //If fields are empty return with error
        if (empty($emailphone) || empty($password)) {
            $emptyErr = "All fields need to be populated!";
            $validations['emptyErr'] = $emptyErr;
        }

        //Begin query to check if user exists
        $userQ = "SELECT * FROM users WHERE userEmail = ? OR userPhoneNumber = ?;";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $userQ)) {
            $InternErr = "Internal Error";
            $validations['InternErr'] = $InternErr;
        }
        //use binding to prevent SQL injection
        mysqli_stmt_bind_param($stmt, "ss", $emailphone, $emailphone);
        mysqli_stmt_execute($stmt);

        $result = mysqli_stmt_get_result($stmt);
        $row = mysqli_fetch_assoc($result);
        //if query returns a row then user exists, else return with error
        $oldEmail = $emailphone;
        if ($row) {
            /*
         * Reasoning for a if else if for a boolean check is to make sure that in the case there is an error in
         * code and the password_verify function fails for some reason the user is not logged in if the password
         * is not verified
         */
            //if password is not verified return error

            if (!password_verify($password, $row['userPassword'])) {
                $emailPassErr = "Check your Email or Password!";
                $validations['emailPassErr'] = $emailPassErr;
                //$password = password_hash($password, PASSWORD_DEFAULT);
                //if($password !== $row['userPassword']){

            } else if (password_verify($password, $row['userPassword'])) {
                //password is verified start session and assign session variables
                session_start();
                $_SESSION['id'] = $row['userId'];
                $_SESSION['name'] = $row['userName'];
                $_SESSION['right'] = $row['userRight'];
                $_SESSION['email'] = $row['userEmail'];
                $oldEmail = "";
                header("Location: ./account.php");
                //exit();
            }
        } else {
            $emailPassErr = "Check your Email or Password!";
            $validations['emailPassErr'] = $emailPassErr;
        }
        echo $twig->render('login.html', [
            'validations' => $validations,
            'oldEmail' => $oldEmail
        ]);
    } else {
        header("Location: ..");
    }
} else {
    echo $twig->render('login.html');
}
