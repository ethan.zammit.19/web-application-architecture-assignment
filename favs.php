<?php
if (!isset($_SESSION))
    session_start();
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
require_once "includes/dbc.inc.php";
$error = "";
$logged = $menu = $catID = -1;
$Categories = $MenuItems = $FavItems = $FavMenuItems = [];
if (!isset($_SESSION['id'])) {
    echo $twig->render('403.html');
    exit();
}
$logged = 1;
//this is basically a sub-part of the index, i copied some functions, and thus wont explain them, as they are already explained in the index earlier. 



$sql = "SELECT * FROM favourites WHERE userID = " . $_SESSION['id'] . ";";
$stmt = mysqli_stmt_init($conn);

if (!mysqli_stmt_prepare($stmt, $sql)) {
    $error .= "Internal Error Encountered!\n";

    echo $twig->render(
        'favs.html',
        ['error' => $error]
    );
    exit();
}
mysqli_stmt_execute($stmt);
$result = mysqli_stmt_get_result($stmt);
$i = 0;
while ($row = mysqli_fetch_assoc($result)) {
    $FavItems[$i]["id"] = $row["itemID"];
    $i++;
}
for ($j = 0; $j < $i; $j++) {
    $sql = "SELECT * FROM menuitem WHERE itemId = " . $FavItems[$j]["id"] . ";";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        $error .= "Internal Error Encountered!\n";

        echo $twig->render(
            'favs.html',
            ['error' => $error]
        );
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);
    $FavMenuItems[$j]["id"] = $row["itemId"];
    $FavMenuItems[$j]["name"] = $row["itemName"];
    $FavMenuItems[$j]["image"] = $row["itemImage"];
    $FavMenuItems[$j]["desc"] = $row["itemDesc"];
    $FavMenuItems[$j]["price"] = $row["itemPrice"];
    $FavMenuItems[$j]["spice"] = $row["itemSpice"];
    $FavMenuItems[$j]["allergy"] = $row["itemAllergy"];
    $FavMenuItems[$j]["delete"] = $row["itemDelete"];
}
$success = "";
//check if the user has submitted the form to send his/her favourite list
if (isset($_POST['recip'])) {
    require_once(__DIR__ . '/includes/mailer.inc.php');
    $mail->addAddress($_POST['recip']);

    $mail->Subject = $_SESSION['name'] . "'s Favourites list";
    //we can add more things to send, yet this is a simple implementation!
    for ($j = 0; $j < $i; $j++) {
        $mail->Body .= "Item Name:"  . $FavMenuItems[$j]["name"] . "\n";
    }

    try {
        //send
        $mail->send();
    } catch (\PHPMailer\PHPMailer\Exception $e) {
        echo "Something Went Wrong!\nPlease try again!";
        echo $twig->render('500.html');
        exit();
    }
    $success = "Action Completed Successfully!";
}
//keeping loading page anyway


echo $twig->render(
    'favs.html',
    [
        'logged' => $logged,
        'success' => $success,
        'favmenu' => $FavMenuItems
    ]
);
