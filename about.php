<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
require_once "includes/dbc.inc.php";
// Initialise variables
$OpeningHours = [];
$restaurant = $owner = -1;

//Check no error have been made
if(!isset($_Get['error'])){
    // Using mysqli load opening hours into the array
    $sql = "SELECT * FROM openinghours";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("Location: ../about.php?error=sqlerror1");
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        if($row['timeDelete'] != 1){
            $OpeningHours[$i]['id'] = $row["timeID"];
            $OpeningHours[$i]['day'] = $row["timeDate"];
            $OpeningHours[$i]['start'] = $row["timeStart"];
            $OpeningHours[$i]["end"] = $row["timeEnd"];
            $i++;
        }
    }

    // Load contents of RestaurantData.txt into restaurant and OwnerDetails.txt into owner
    $restaurant = file_get_contents(__DIR__."/config/RestaurantData.txt");
    $owner = file_get_contents(__DIR__."/config/OwnerDetails.txt");
}

echo $twig->render('/editable/about.html',
    [   'restaurantdata' => $restaurant,
        'ownerdetails' => $owner,
        'openingHours' => $OpeningHours
    ]);
