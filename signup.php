<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';


if (isset($_POST['submit']) && ($_SERVER['REQUEST_METHOD'] === 'POST')) {
    require_once 'includes/dbc.inc.php';

    $fullname = trim($_POST['name']);
    $email = trim($_POST['email']);
    $password =  trim($_POST['pass']);
    $phone = trim($_POST['phone']);

    $emptyErr =  $emailPassErr = $passErr = $taken = $InternErr = "";


    // '../' to go back a directory where I to put the php files in a folder (probabli)
    // some error handling
    $err = false;

    $old['email'] = $_POST['email'];
    $old['name'] =  $_POST['name'];
    $old['phone'] = $_POST['phone'];
    // error checking
    if (empty($fullname) || empty($email) || empty($password) || empty($phone)) {

        $emptyErr = "Kindly Fill all the Fields\n";
        $err = true;
        $validation['emptyErr'] = $emptyErr;
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $err = true;
        $emailPassErr = "Kindly choose a valid email\n";
        $validation['emailErr'] = $emailPassErr;
    } else if (!filter_var($phone, FILTER_VALIDATE_INT)) {
        $err = true;
        $emailPassErr = "Kindly choose a valid Phone Number\n";
        $validation['phoneErr'] = $emailPassErr;
    }
    if ($_POST['pass'] !== $_POST['cpass']) {
        $err = true;
        $passErr = "Your passwords do not match\n";
        $validation['passErr'] = $passErr;
    }


    $userQ = "SELECT * FROM users WHERE userEmail = ? OR userPhoneNumber = ?;";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $userQ)) {
        echo $twig->render('500.html');
        exit();
    }

    mysqli_stmt_bind_param($stmt, "ss", $email, $phone);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_store_result($stmt);

    //if there are more than 0 rows (only 1 is allowed) then either the phone or email is already taken
    //not sure whether to specify or not
    if (mysqli_stmt_num_rows($stmt) > 0) {
        $err = true;
        $taken = "An account is already associated with that email or phone\n";
        $validation['taken'] = $taken;
    }

    if ($err) {
        echo $twig->render('signup.html', [
            'validations' => $validation,
            'old' => $old
        ]);
        exit();
    }

    //create sign up SQL
    $signUp = "INSERT INTO users (userName, userEmail, userPhoneNumber, userPassword) VALUES (?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $signUp)) {
        echo $twig->render('500.html');
        exit();
    }

    //execute SQL with binding to prevent SQL injection
    mysqli_stmt_bind_param(
        $stmt,
        "ssss",
        $fullname,
        $email,
        $phone,
        password_hash($password, PASSWORD_DEFAULT)
    );
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($conn);


    $oldEmail = $oldPhone = $oldName = "";
    exit(header("Location: ./account.php"));
} else {

    echo $twig->render('signup.html');
}
