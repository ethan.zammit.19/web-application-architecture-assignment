<?php
session_start();
require_once __DIR__ . '/bootstrap.php';
// $active = 'contact.php';
require_once __DIR__ . '/navbar.php';
//check if uer is logged or not
$logged = false;
if (isset($_SESSION['id'])) {
    $logged = true;
}
// check if it is a booking
if (!isset($book))
    $book = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_POST["type"]))
        $type = "booking";
    else
        $type = $_POST["type"];

    $sub = $_POST["subject"];
    $fname = $_POST["fname"];
    $email = $_POST["email"];


    if ($logged == true) {
        $fname = $_SESSION["name"];
        $email = $_SESSION["email"];
    }

    require_once('./includes/mailer.inc.php');
    try {
        $mail->Subject = 'Receipt of your ' . $type;
        if ($type == "booking")
            $mail->Body    = 'Hi, ' . $fname . ' we have successfully received your ' . $type . "\nDate: " . $_POST['date'] . "\nTime: " . $_POST['time'] . "\nAttendees: " . $_POST['attendees'];
        else
            $mail->Body    = 'Hi, ' . $fname . ' We have received your ' . $type . ' and will answer you as soon as we can!' . "\r\n\"" . $sub . '"';


        $mail->addAddress($email);
        $mail->send();
    } catch (\PHPMailer\PHPMailer\Exception $e) {

        echo "Something Went Wrong!\nPlease try again!";
        echo $twig->render('contactus.html', ['book' => $book, 'logged' => $logged]);
    }
    $mail->ClearAllRecipients();

    $mail->addAddress("burgerbarn-mt@outlook.com");
    try {
        $mail->Subject = 'New ' . $type . ' received';
        if ($type == "booking") {
            $mail->Body    = 'Client: ' . $fname . "\nDate: " . $_POST['date'] . "\nTime: " . $_POST['time'] . "\nAttendees: " . $_POST['attendees'];
        } else {
            $mail->Body    = 'Client: ' . $fname . "\r\nContent:\r\n\"" . $sub . '"';
        }
        $mail->send();
    } catch (\PHPMailer\PHPMailer\Exception $e) {
        echo "Something Went Wrong!\nPlease try again!";
        echo $twig->render('contactus.html', ['book' => $book, 'logged' => $logged]);
    }
    $mail->ClearAllRecipients();

    header("Location: index.php");
    exit();
} else
    echo $twig->render('contactus.html', ['book' => $book, 'logged' => $logged]);
