<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/includes/navbar_xml_loader.inc.php';
echo $twig->render('navbar.html', ['navs' => $Nav, 'count' => $count]);
