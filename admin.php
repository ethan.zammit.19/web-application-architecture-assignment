<?php
session_start();
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
$Categories = $MenuItems = $Pages = $openinghours = [];
$categoryID = $menuID = $validation = $pageID = $greeting = $owner = $restaurant = -1;

//Make sure an admin accessed this fie
if (isset($_SESSION)) {
    if ($_SESSION['right'] != 1) {
        header("Location: account.php");
        exit();
    }
} else {
    header("Location: account.php");
    exit();
}
// se validation according to what form was pressed
if (isset($_POST["addCategory"])) {
    $validation = 1;
}
if (isset($_POST["editCategory"])) {
    $validation = 2;
}
if (isset($_POST["deleteCategory"])) {
    $validation = 3;
}
if (isset($_POST["addMenu"])) {
    $validation = 4;
}
if (isset($_POST["editMenu"])) {
    $validation = 5;
    $menuID = $_POST["menuid"];
}
if (isset($_POST["deleteMenu"])) {
    $validation = 6;
    $menuID = $_POST["menuid"];
}
if (isset($_POST["addDate"])) {
    $validation = 7;
    $pageID = $_POST["pageid"];
}
if (isset($_POST["deleteDate"])) {
    $validation = 8;
    $pageID = $_POST["pageid"];
}


$error = "";
require_once "includes/dbc.inc.php";

// If main "menu" option was chosen or a category or a form that requires category data (any form related to categories and menu items)
if ((isset($_POST["ExpandOnMenu"]) || isset($_POST["category"]) || $validation !== -1) && $validation < 7) {
    //Show all categories
    $categoryID = -2;
    //Load all categories fromthe database
    $sql = "SELECT * FROM menuCategory";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        $error .= "Internal Error Encountered!\n";

        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    // Load the necessary data into the categories array
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $Categories[$i]["id"] = $row["itemCategoryID"];
        $Categories[$i]["name"] = $row["categoryName"];
        //$Categories[$i]["image"] = $row["categoryImagePath"];
        //$Categories[$i]["desc"] = $row["categoryDescription"];
        $Categories[$i]["delete"] = $row["categoryDelete"];
        $i++;
    }
}
// If a category was chosen (or already chosen) make sure to only show that and not the others
if (isset($_POST["categoryid"])) {
    $categoryID = $_POST["categoryid"];
}

//A category or one of the menu forms was chosen
if (isset($_POST["category"]) || $validation > 3 && $validation < 7) {
    //check which category was chosen
    foreach ($Categories as $category) {
        if ($_POST["categoryid"] == $category["id"]) {
            // Load the menu items from the chosen category from the db
            $sql = "SELECT * FROM menuitem WHERE itemCategoryID = " . $category["id"] . ";";
            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt, $sql)) {
                $error .= "Internal Error Encountered!\n";

                echo $twig->render(
                    'admin.html',
                    ['error' => $error]
                );
                exit();
            }
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $i = 0;
            // Load the necessary data into the MenuItems array
            while ($row = mysqli_fetch_assoc($result)) {
                $MenuItems[$i]["id"] = $row["itemId"];
                $MenuItems[$i]["name"] = $row["itemName"];
                //$MenuItems[$i]["image"] = $row["itemImage"];
                //$MenuItems[$i]["desc"] = $row["itemDesc"];
                //$MenuItems[$i]["price"] = $row["itemPrice"];
                //$MenuItems[$i]["spice"] = $row["itemSpice"];
                //$MenuItems[$i]["allergy"] = $row["itemAllergy"];
                $MenuItems[$i]["delete"] = $row["itemDelete"];
                $i++;
            }
        }
    }
}

//If the main "pages" option was selected, a page to be edited or a form related to one of the pages selected
if (isset($_POST["ExpandOnPages"]) || isset($_POST["page"]) || $validation > 6) {
    // set page names
    $Pages[0] = "Home Page";
    $Pages[1] = "About Us";
    // check that a page was selected
    if (isset($_POST["page"]) || isset($_POST["pageid"])) {
        $pageID = $_POST["pageid"];
        if ($pageID == 0) {
            // Load greeting if home page was selected
            $greeting = file_get_contents(__DIR__ . "/config/Greeting.txt");
        }
        if ($pageID == 1) {
            // load txt files if about us was selected
            $restaurant = file_get_contents(__DIR__ . "/config/RestaurantData.txt");
            $owner = file_get_contents(__DIR__ . "/config/OwnerDetails.txt");
            // load all opening hours from the db
            $sql = "SELECT * FROM openinghours";
            $stmt = mysqli_stmt_init($conn);

            if (!mysqli_stmt_prepare($stmt, $sql)) {
                $error .= "Internal Error Encountered!\n";
                $halt = true;
                echo $twig->render(
                    'admin.html',
                    ['error' => $error, 'halt' => $halt]
                );
                exit();
            }
            mysqli_stmt_execute($stmt);
            $result = mysqli_stmt_get_result($stmt);
            $i = 0;
            // Load the necessary data into the openinghours array
            while ($row = mysqli_fetch_assoc($result)) {
                $openinghours[$i]["id"] = $row["timeID"];
                $openinghours[$i]["day"] = $row["timeDate"];
                //$openinghours[$i]["start"] = $row["timeStart"];
                //$openinghours[$i]["end"] = $row["timeEnd"];
                //$openinghours[$i]["delete"] = $row["timeDelete"];
                $i++;
            }
        }
    }
}
$success = "Action Completed Successfully!";
// render with all the variable, if a variable was not affected on a particular run it has a default value.
echo $twig->render(
    'admin.html',
    [
        //All Pages
        'validation' => $validation,
        'success' => $success,
        // Menu
        'Categories' => $Categories,
        'MenuItem' => $MenuItems,
        'menuID' => $menuID,
        'categoryid' => $categoryID,
        // Page
        'Pages' => $Pages,
        'pageid' => $pageID,
        'Greeting' => $greeting,
        'restaurantdata' => $restaurant,
        'ownerdetails' => $owner,
        'openinghours' => $openinghours
    ]
);
