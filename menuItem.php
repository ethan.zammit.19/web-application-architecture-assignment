<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
require_once "includes/dbc.inc.php";
session_start();
// make sure the user came from the menu page
if (isset($_POST['menuid'])) {
    $menID = $_POST['menuid'];
    $error = "";
    $userID = $favourite = -1;
    $MenuItems = [];
    // start loading the menu item from the db
    $sql = "SELECT * FROM menuitem WHERE itemId = " . $menID . ";";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $i = 0;
    if ($row = mysqli_fetch_assoc($result)) {
        $MenuItems["id"] = $row["itemId"];
        $MenuItems["name"] = $row["itemName"];
        $MenuItems["image"] = $row["itemImage"];
        $MenuItems["desc"] = $row["itemDesc"];
        $MenuItems["price"] = $row["itemPrice"];
        $MenuItems["spice"] = $row["itemSpice"];
        $MenuItems["allergy"] = $row["itemAllergy"];
        //$MenuItems["delete"] = $row["itemDelete"];
    }
    //check if the item is favourited or not
    $sql = "SELECT * FROM favourites WHERE itemId = " . $menID . ";";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        $error .= "Internal Error Encountered!1\n";

        echo $twig->render(
            'menuItem.html',
            ['error' => $error]
        );
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    if ($row = mysqli_fetch_assoc($result)) {
        // give a different value from the default
        $favourite = 1;
    }
    if (isset($_SESSION['id'])) {
        // give the user id if the user is logged in
        $userID = $_SESSION['id'];
    }
    echo $twig->render(
        'menuItem.html',
        [
            'menu' => $MenuItems,
            'userid' => $userID,
            'favourite' => $favourite
        ]
    );
}
