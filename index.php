<?php
session_start();
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
require_once "includes/dbc.inc.php";
$error = "";
$logged = $menu = $catID = -1;
$Categories = $MenuItems = $FavItems = $FavMenuItems = [];
// start loading the categories
$sql = "SELECT * FROM menuCategory";
$stmt = mysqli_stmt_init($conn);

if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo $twig->render('500.html');
    exit();
}
mysqli_stmt_execute($stmt);
$result = mysqli_stmt_get_result($stmt);
$i = 0;
// load all the info into the categories array
while ($row = mysqli_fetch_assoc($result)) {
    $Categories[$i]["id"] = $row["itemCategoryID"];
    $Categories[$i]["name"] = $row["categoryName"];
    $Categories[$i]["image"] = $row["categoryImagePath"];
    $Categories[$i]["desc"] = $row["categoryDescription"];
    $Categories[$i]["delete"] = $row["categoryDelete"];
    $i++;
}
//if a category is selected
if (isset($_POST['categoryid'])) {
    //load the menu items from that category
    $sql = "SELECT * FROM menuitem WHERE itemCategoryID = " . $_POST['categoryid'] . ";";
    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    }
    mysqli_stmt_execute($stmt);
    $result = mysqli_stmt_get_result($stmt);
    $i = 0;
    // load into menuitems
    while ($row = mysqli_fetch_assoc($result)) {
        $MenuItems[$i]["id"] = $row["itemId"];
        $MenuItems[$i]["name"] = $row["itemName"];
        $MenuItems[$i]["image"] = $row["itemImage"];
        $MenuItems[$i]["desc"] = $row["itemDesc"];
        $MenuItems[$i]["price"] = $row["itemPrice"];
        $MenuItems[$i]["spice"] = $row["itemSpice"];
        $MenuItems[$i]["allergy"] = $row["itemAllergy"];
        $MenuItems[$i]["delete"] = $row["itemDelete"];
        $i++;
    }
    // indicate a menu has been selected and its category id
    $menu = 1;
    $catID = $_POST['categoryid'];
}
//load greeting
$greeting = file_get_contents(__DIR__ . "/config/Greeting.txt");

$success = "Action Completed Successfully!";
echo $twig->render(
    '/editable/index.html',
    [
        'logged' => $logged,
        'success' => $success,
        'categories' => $Categories,
        'catID' => $catID,
        'ismenu' => $menu,
        'menu' => $MenuItems,
        'favmenu' => $FavMenuItems,
        'greetingMSG' => $greeting
    ]
);
