<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';
session_start();


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $errors = $success =  "";

    if (isset($_POST["pwd-reset-submit"])) {
        $selector = $_GET["selector"];
        $validator = $_GET["validator"];
        $password = $_POST["pwd"];
        $passwordRepeat = $_POST["pwd-repeat"];
        if (empty($password) || empty($passwordRepeat)) {
            $errors .= "Kindly fill all fields\n";
        } else if ($password != $passwordRepeat) {
            $errors .= "Passwords do not match\n";
        }
        if ($errors != "") {
            echo $twig->render('create-new-password.html', ['errors' => $errors, 'val' => $_GET['validator'], 'sel' => $_GET['selector']]);
            exit();
        }

        $currentDate = date("U");

        require_once './includes/dbc.inc.php';

        $sql = "SELECT * FROM pwdReset WHERE pwdResetSelector=? AND pwdResetExpires >= '.$currentDate.';";
        $stmt = mysqli_stmt_init($conn);
        echo $twig->render('500.html');
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo $twig->render('500.html');
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $selector);
            mysqli_stmt_execute($stmt);

            $result = mysqli_stmt_get_result($stmt);
            $row = mysqli_fetch_assoc($result);
            if (!$row) {
                $errors .= "Kindly resubmit your request\n";
            } else {
                $tokenBin = hex2bin($validator);

                if (!password_verify($tokenBin, $row["pwdResetToken"])) {
                    echo $twig->render('500.html');
                    exit();
                } elseif (password_verify($tokenBin, $row["pwdResetToken"])) {
                    $tokenEmail = $row['pwdResetEmail'];

                    $sql = "SELECT * FROM users WHERE userEmail=?;";
                    $stmt = mysqli_stmt_init($conn);
                    if (!mysqli_stmt_prepare($stmt, $sql)) {
                        $errors .= "Kindly resubmit your request\n";
                    } else {
                        mysqli_stmt_bind_param($stmt, "s", $tokenEmail);
                        mysqli_stmt_execute($stmt);
                        $result = mysqli_stmt_get_result($stmt);
                        if (!$row = mysqli_fetch_assoc($result)) {
                            echo $twig->render('500.html');
                            exit();
                        } else {
                            $sql = "UPDATE users SET userPassword=? WHERE userEmail=?;";
                            if (!mysqli_stmt_prepare($stmt, $sql)) {
                                echo $twig->render('500.html');
                                exit();
                            } else {
                                $newPwdHash = password_hash($password, PASSWORD_DEFAULT);
                                mysqli_stmt_bind_param($stmt, "ss", $newPwdHash, $tokenEmail);
                                mysqli_stmt_execute($stmt);

                                $sql = "DELETE FROM pwdReset WHERE pwdResetEmail=?;";

                                if (!mysqli_stmt_prepare($stmt, $sql)) {
                                    echo $twig->render('500.html');
                                    exit();
                                } else {
                                    mysqli_stmt_bind_param($stmt, "s", $tokenEmail);
                                    mysqli_stmt_execute($stmt);
                                    mysqli_stmt_close($stmt);
                                    mysqli_close($conn);
                                    $success = "Password Updated Succesfully!\n";
                                    echo $twig->render('create-new-password.html', ['success' => $success]);
                                    exit();
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($errors != "")
            echo $twig->render('403.html', ['errors' => $errors]);
        else
            echo $twig->render('403.html');
        exit();
    }
} else {
    if (empty($_GET['selector']) || empty($_GET['validator'])) {
        echo $twig->render('403.html');
        exit();
    }
    echo $twig->render('create-new-password.html', ['val' => $_GET['validator'], 'sel' => $_GET['selector']]);
}
