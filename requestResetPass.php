<?php
require_once __DIR__ . '/bootstrap.php';
require_once __DIR__ . '/navbar.php';

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    //Make sure that the php script was accessed from the button
    require_once './includes/dbc.inc.php';
    $selector = bin2hex(random_bytes(8));
    $token = random_bytes(32);

    $url = getenv('HTTP_HOST') . "/ChooseNewPass.php?selector=" . $selector . "&validator=" . bin2hex($token);
    //time to change password is 15 minutes
    $expires = date("U") + 450;
    //start email query to get email from user and to check whether user exists or not
    $emailQ = "SELECT * FROM users WHERE userEmail = ? OR userPhoneNumber = ?;";
    $stmt = mysqli_stmt_init($conn);

    $validation = $success = "";
    $failed = false;


    //ceheck for sql errors
    if (!mysqli_stmt_prepare($stmt, $emailQ)) {
        $validation .= ("An internal Error has occured, please try again later\n");
        $failed = true;
    }
    $email = trim($_POST['emailphone']);
    // bind to prevent sql injection
    mysqli_stmt_bind_param($stmt, "ss", $email, $email);
    mysqli_stmt_execute($stmt);

    $result = mysqli_stmt_get_result($stmt);
    $row = mysqli_fetch_assoc($result);

    // check if the user exists
    if ($row) {
        $emailphone = $row['userEmail'];
        $exists = true;
    } else {
        $exists = false;
    }

    if (!$failed) {

        //write SQL to delete any tokens created in the db (if any) to avoid having 2 tokens open at the same time
        $deleteToken = "DELETE FROM pwdreset WHERE pwdresetEmail = ?;";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $deleteToken)) {
            $validation .= ("An internal Error has occured, please try again later\n");
            $failed = true;
        }
        mysqli_stmt_bind_param($stmt, "s", $emailphone);
        mysqli_stmt_execute($stmt);

        //write SQL to insert a token into the pwdReset table
        $insertToken = "INSERT INTO pwdReset (pwdResetEmail, pwdResetSelector, pwdResetToken, pwdResetExpires) VALUES (?, ?, ?, ?);";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $insertToken)) {
            $validation .= ("An internal Error has occured, please try again later\n");
            $failed = true;
        }
        //hash the token before putting it into the db
        $hashToken = password_hash($token, PASSWORD_DEFAULT);
        mysqli_stmt_bind_param($stmt, "ssss", $emailphone, $selector, $hashToken, $expires);
        mysqli_stmt_execute($stmt);

        mysqli_stmt_close($stmt);
        mysqli_close($conn);


        if (!$failed) {
            // send email to user so that they can be able to change the password
            $href = "<a href=\"" . $url . "\">this link</a>";
            if ($exists) {
                require "./includes/mailer.inc.php";
                #subject to change
                $mail->addAddress($emailphone);
                $mail->isHTML(True);
                $mail->Subject = "BurgerBarn account password reset";
                $mail->Body = "A Password reset was requested for this account. <br>If It wasn't you, kindly contact us for further Guidance.<br>If you would like to reset your account simply follow ";
                $mail->Body .= "$href";
                $mail->send();
            }

            $success = "If a user is associated with that account an email with further instructions will be sent shortly\n";
            echo $twig->render('reset-password.html', ['success' => $success]);
            exit();
        }
    }
    if ($failed)
        echo $twig->render('reset-password.html', ['validations' => $validation]);
} else
    echo $twig->render('reset-password.html');
