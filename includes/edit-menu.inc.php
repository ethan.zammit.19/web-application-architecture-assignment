<?php
// Check that the user got here from the admin page
if (isset($_POST["menedit"])) {
    require_once "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Begin sql with the table name and operation
    $sql = "UPDATE menuitem SET ";
    $stmt = mysqli_stmt_init($conn);

    /*
     * Write sql code piece by piece depending on what the user inputted
     *
     * Binding was deemed not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     *
     * Also I do not know how to make use of the function and fill it dynamically
     */

    $image_path = "";
    if ($name = $_POST['name']) {
        if ($sql !== "UPDATE menuitem SET ") $sql .= ", ";
        $sql .= "itemName='" . $name . "' ";
    }
    if ($_FILES['image']['tmp_name']) {
        $uploadfile = __DIR__ . "/../img/menu/" . basename($_FILES['image']['name']); // maybe make this a function or an include file

        // If a file with the same name already exists delete it and re upload
        if (file_exists($uploadfile)) {
            unlink($uploadfile);
        }

        // If uploading fails return user back to admin
        if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
            $error = "Download Error Occured!\n";
            echo $twig->render(
                '500.html',
                ['error' => $error]
            );
            exit();
        }

        // Add image edit
        $uploadfile = "/img/menu/" . basename($_FILES['image']['name']);
        $sql .= "itemImage = \"" . $uploadfile . "\" ";
    }

    // Add description edit
    if ($description = $_POST['description']) {
        if ($sql !== "UPDATE menuitem SET ") $sql .= ", ";
        $sql .= "itemDescription='" . $description . "' ";
    }

    // Add price edit
    if ($price = $_POST['price']) {
        if ($sql !== "UPDATE menuitem SET ") $sql .= ", ";
        $sql .= "itemPrice='" . $price . "' ";
    }

    // Add allergy edit
    if ($allergy = $_POST['allergy']) {
        if ($sql !== "UPDATE menuitem SET ") $sql .= ", ";
        $sql .= "itemAllergy='" . $allergy . "' ";
    }

    // Add spice edit
    if ($spice = $_POST['spice']) {
        if ($sql !== "UPDATE menuitem SET ") $sql .= ", ";
        $sql .= "itemSpice='" . $spice . "' ";
    }

    // Finalise sql
    $sql .= "WHERE itemID=" . $_POST['menuid'] . ";";

    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        $error = "Internal Error Encountered!\n";

        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    } else {
        // Execute the sql code return to admin
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        header("Location: ../admin.php?edit=success");
    }
} else {
    header("Location: ../index.php");
    exit();
}
