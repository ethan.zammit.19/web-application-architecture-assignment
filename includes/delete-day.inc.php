<?php
// Check that the user got here from the admin page
if (isset($_POST["daydelete"])) {
    require "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Initialised variables that will be used. After checking if they are set

    if(!isset($_POST['delete']) || !isset( $_POST['day'])){
        $error = "Fields left empty\n";
        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }

    $deleteSetting = $_POST['delete'];
    $day =  $_POST['day'] ;

    /*
     * Write sql code to update opening hours delete setting (1 is hide, 0 is show)
     *
     * Binding deemed was not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     */

    $sql = "UPDATE openinghours SET timeDelete=" . $deleteSetting . " WHERE timeID =" . $day . ";";
    require_once "delete-check.inc.php";
} else {
    header("Location: ../index.php");
    exit();
}
