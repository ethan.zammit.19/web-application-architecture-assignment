<?php
require_once 'dbc.inc.php';
require_once '../bootstrap.php';

if (!isset($_POST['userid']) || !isset($_POST['menuid'])) {
    $error = "Fields left empty\n";
    $halt = true;
    echo $twig->render(
        'menuItem.html',
        ['error' => $error, 'halt' => $halt]
    );
    exit();
}

$user = $_POST['userid'];
$menu =  $_POST['menuid'];

//Check that the user got here from the menu item page
if (isset($_POST['favourite'])) {
    /*
     * Since there is no need for a delete column in favourites so here we write the sql code
     * depending on the whether the item is a favourite or not
     */
    if ($_POST['favourite'] == 2) {
        $sql = "INSERT INTO favourites (userID, itemID) VALUES (" . $user . "," . $menu . ");";
    } else {
        $sql = "DELETE FROM favourites WHERE userID = " . $user . " AND itemID = " . $menu . ";";
    }
    $stmt = mysqli_stmt_init($conn);
    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    }
    // Execute the sql code return to the menu
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
    mysqli_close($conn);

    Header("Location: ../index.php");
} else {
    Header("Location: ../index.php");
}
