<?php
// Check that the user got here from the admin page
if (isset($_POST["addDate"])) {
    require_once "dbc.inc.php";
    require_once '../bootstrap.php';

    // Initialised variables that will be used. After checking if they are set

    if (!isset($_POST['day']) || !isset($_POST['start']) || !isset($_POST['end'])) {
        $error = "Fields left empty\n";

        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }

    $day = $_POST['day'];
    $start = $_POST['start'];
    $end = $_POST['end'];

    // Write sql code to insert new opening hours

    $sql = "INSERT INTO openinghours (timeDate, timeStart, timeEnd) VALUES (?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);

    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    } else {
        // Bind the variables initialised earlier to avoid sql injection
        mysqli_stmt_bind_param($stmt, "sss", $day, $start, $end);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        //Take user back to the previous page with no errors
        header("Location: ../admin.php");
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
