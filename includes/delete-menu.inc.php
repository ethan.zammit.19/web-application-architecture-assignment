<?php
// Check that the user got here from the admin page
if (isset($_POST["mendelete"])) {
    require_once "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Initialised variables that will be used. After checking if they are set

    if(!isset($_POST['delete']) || !isset( $_POST['menuid'])){
        $error = "Fields left empty\n";
        $halt = true;
        echo $twig->render('admin.html', ['error' => $error, 'halt' => $halt]);
        exit();
    }

    $deleteSetting = $_POST['delete'];
    $menu =  $_POST['menuid'] ;

    /*
     * Write sql code to update menu delete setting (1 is hide, 0 is show)
     *
     * Binding was deemed not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     */


    $sql = "UPDATE menuitem SET itemDelete=" . $deleteSetting . " WHERE itemID =" . $menu . ";";
    $stmt = mysqli_stmt_init($conn);
    // Copy paste the code as it is invariant to the variables used and is common with all delete scripts
    require_once "delete-check.inc.php";
} else {
    header("Location: ../index.php");
    exit();
}
