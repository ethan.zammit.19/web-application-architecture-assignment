<?php
require_once __DIR__ . '/../bootstrap.php';
$error = "";
// Check that the user got here from the admin page
if (isset($_POST["menadd"])) {
    //Make sure that an image uploaded
    if (isset($_FILES["image"]))
        $check = getimagesize($_FILES["image"]["tmp_name"]);
    else {
        $error = "Menu item Add Requires An Image!\n";
        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }

    //if the file was not an image display an error
    if ($check !== false) {
        $image = basename($_FILES['image']['name']);
        $uploadfile = __DIR__ . "/../img/menu/" . $image;
        // If a file with the same name already exists delete it and re upload

        //In comparison to the edit scripts this is done to avoid overwriting on different menu items' images
        if (file_exists($uploadfile)) {
            $error = "File ALready Present!\n";
            $halt = true;
            echo $twig->render(
                'admin.html',
                ['error' => $error, 'halt' => $halt]
            );
            exit();
        }

        // Initialise and do some error checking

        require_once "dbc.inc.php";
        $name = $_POST['name'];
        $description = $_POST['description'];
        $price = $_POST['price'];
        $allergy = $_POST['allergy'];
        $spice = $_POST['spice'];

        if (empty($name) || empty($price) || empty($description)) {
            $error = "Field cannot be empty!\n";
            $halt = true;
            echo $twig->render(
                'admin.html',
                ['error' => $error, 'halt' => $halt]
            );
            exit();
        }

        //Write sql to check if a  category with the same name already exists or not
        $sql = "SELECT * FROM menuitem WHERE itemName = ?;";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo $twig->render('500.html');
        }

        //Bind the statement and execute
        mysqli_stmt_bind_param($stmt, "s", $name);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        //If the statement returns any rows return the user with an error message
        if (mysqli_stmt_num_rows($stmt) > 0) {
            header("Location: ../admin.php?error=namealreadytaken");
            exit();
        }

        //Write actual sql to insert a new row
        $sql = "INSERT INTO menuitem (itemName, itemImage, itemDesc, itemPrice, itemSpice, itemAllergy, itemCategoryID) VALUES (?, ?, ?, ?, ?, ?, ?);";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo $twig->render('500.html');
        }

        // Do error checking for image
        if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
            $error = "Download Error Occured!\n";
            echo $twig->render(
                '500.html',
                ['error' => $error]
            );
            exit();
        }
        // change directory so that src can load image
        $uploadfile = "/img/menu/" . $image;
        //execute SQL with binding to prevent SQL injection
        mysqli_stmt_bind_param($stmt, "ssssssd", $name, $uploadfile, $description, $price, $spice, $allergy, $_POST['categoryid']);
        mysqli_stmt_execute($stmt);
        header("Location: ../admin.php?success=itemadded");
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        exit();
    } else {
        $error = "File uploaded is not an image!\n";
        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
