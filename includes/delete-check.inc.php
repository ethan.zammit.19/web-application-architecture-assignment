<?php
$stmt = mysqli_stmt_init($conn);
/*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */
if (!mysqli_stmt_prepare($stmt, $sql)) {
    echo $twig->render('500.html');
    exit();
} else {
    // Execute the sql as there was no binding
    mysqli_stmt_execute($stmt);
    //Take user back to the previous page with no errors
    header("Location: ../admin.php?success=itemdeleted");
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
    exit();
}
