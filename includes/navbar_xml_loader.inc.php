<?php
require_once __DIR__ . '/../bootstrap.php';

//Reading Xml File
$NavItems = simplexml_load_file(__DIR__ . '../../config/Navigation.xml') or die("XML file could not be loaded!");
$count = 0;
$Nav = [];
foreach ($NavItems as $item) {
    $Nav[$count] = $item->name;
    $Nav[$count + 1] = $item->link;
    $count += 2;
}
