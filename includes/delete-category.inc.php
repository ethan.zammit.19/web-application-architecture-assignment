<?php
// Check that the user got here from the admin page
if (isset($_POST["catdelete"])) {
    require_once "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Initialised variables that will be used. After checking if they are set

    if (!isset($_POST['delete']) || !isset($_POST['categoryid'])) {
        $error = "Fields left empty\n";
        $halt = true;
        echo $twig->render('admin.html',['error' => $error, 'halt' => $halt]);
        exit();
    }

    $deleteSetting = $_POST['delete'];
    $categoryID = $_POST['categoryid'];

    /*
     * Write sql code to update category delete setting (1 is hide, 0 is show) for a specific category
     *
     * Binding was deemed not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     */

    $sql = "UPDATE menucategory SET categoryDelete=" . $deleteSetting . " WHERE itemCategoryID =" . $categoryID . ";";
    $stmt = mysqli_stmt_init($conn);

    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    } else {
        mysqli_stmt_execute($stmt);
    }

    // Write sql code to update menu delete setting for all the menu items in a category

    $sql = "UPDATE menuitem SET itemDelete=" . $deleteSetting . " WHERE itemCategoryID =" . $categoryID . ";";

    // Copy paste the code as it is invariant to the variables used and is common with all delete scripts
    require_once "delete-check.inc.php";
} else {
    header("Location: ../index.php");
    exit();
}
