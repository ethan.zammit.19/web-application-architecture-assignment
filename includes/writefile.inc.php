<?php
// make sure that the text field and the user came from the admin page
if (isset($_POST["textEdit"]) && $_POST["content"]) {
    // open the file specified in the form
    $f = fopen('../config/' . $_POST["file"], 'w');
    // overwrite with the new content
    fwrite($f, $_POST["content"]);
    fclose($f);
    header("Location: ../admin.php?success=edit");
} else {
    header("Location: ../index.php");
    exit();
}
