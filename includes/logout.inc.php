<?php
//make sure session exists
session_start();
//remove session details
session_unset();
session_destroy();
//go back to index.html
header("Location: ../index.php");
exit();