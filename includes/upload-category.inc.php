<?php
require_once __DIR__ . '/../bootstrap.php';
// Check that the user got here from the admin page
if (isset($_POST["catadd"])) {
    //Make sure that an image uploaded
    if (isset($_FILES['image'])) {
        //getimagesizes returns an array if the file uploaded is an image
        $check = getimagesize($_FILES["image"]["tmp_name"]);
    } else {
        $error = "Category Addition Requires An Image!\n";
        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }

    //if the file was not an image display an error
    if ($check !== false) {
        $uploadfile = __DIR__ . "/../img/categories/" . basename($_FILES['image']['name']);
        // If a file with the same name already exists delete it and re upload

        //In comparison to the edit scripts this is done to avoid overwriting on different menu items' images
        if (file_exists($uploadfile)) {
            $error = "File Already Present!\n";
            $halt = true;
            echo $twig->render(
                'admin.html',
                ['error' => $error, 'halt' => $halt]
            );
            exit();
        }

        require_once "dbc.inc.php";

        // Error checking is being done from the html side
        $name = $_POST['name'];
        $description = $_POST['description'];

        if (empty($name) ||  empty($description)) {
            $error = "Kindly fill all the fields\n";
            echo $twig->render(
                'admin.html',
                ['error' => $error, 'halt' => true]
            );
            exit();
        }

        //Write sql to check if a  category with the same name already exists or not
        $sql = "SELECT * FROM menucategory WHERE categoryName = ?;";
        $stmt = mysqli_stmt_init($conn);

        if (!mysqli_stmt_prepare($stmt, $sql)) {
            echo $twig->render('500.html');
            exit();
        }
        //Bind the statement and execute
        mysqli_stmt_bind_param($stmt, "s", $name);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_store_result($stmt);
        //If the statement returns any rows return the user with an error message
        if (mysqli_stmt_num_rows($stmt) > 0) {
            $error .= "Name already taken!\n";
            echo $twig->render(
                'admin.html',
                ['error' => $error]
            );
            exit();
        }
        //Write actual sql to insert a new row
        $sql = "INSERT INTO menucategory (categoryName, categoryDescription, categoryImagePath) VALUES (?, ?, ?);";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            $halt = true;
            echo $twig->render(
                'admin.html',
                ['error' => $error, 'halt' => $halt]
            );
            exit();
        }
        // Do error checking for image
        if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
            $error = "Download Error Occured!\n";
            echo $twig->render(
                '500.html',
                ['error' => $error]
            );
            exit();
        }
        // change directory so that src can load image
        $uploadfile = "/img/categories/" . basename($_FILES['image']['name']);

        //execute SQL with binding to prevent SQL injection
        mysqli_stmt_bind_param($stmt, "sss", $name, $description, $uploadfile);
        mysqli_stmt_execute($stmt);
        header("Location: ../admin.php?success=itemadded");
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        exit();
    } else {
        // Take user back and tell them to upload an image
        $error = "File uploaded is not an image!\n";
        $halt = true;
        echo $twig->render(
            'admin.html',
            ['error' => $error, 'halt' => $halt]
        );
        exit();
    }
} else {
    header("Location: ../index.php");
    exit();
}
