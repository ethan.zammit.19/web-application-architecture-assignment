<?php
// Check that the user got here from the admin page
if (isset($_POST["dateEdit"])) {
    require_once "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Begin sql with the table name and operation
    $sql = "UPDATE openinghours SET ";
    $stmt = mysqli_stmt_init($conn);
    /*
     * Write sql code piece by piece depending on what the user inputted
     *
     * Binding was deemed not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     *
     * Also I do not know how to make use of the function and fill it dynamically
     */

    // Add starting time edit
    if ($start = $_POST['start']) {
        if ($sql !== "UPDATE timeStart SET ") $sql .= ", ";
        $sql .= "itemDescription='" . $start . "' ";
    }

    // Add closing time edit
    if ($end = $_POST['end']) {
        if ($sql !== "UPDATE timeEnd SET ") $sql .= ", ";
        $sql .= "itemPrice='" . $end . "' ";
    }

    // Finalise sql
    $sql .= "WHERE timeDate= " . $_POST['day'] . ";";

    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
    } else {
        // Execute the sql code return to admin
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        header("Location: ../admin.php?edit=success");
    }
} else {
    header("Location: ../index.php");
    exit();
}
