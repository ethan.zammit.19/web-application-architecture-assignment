<?php
// Check that the user got here from the admin page
if (isset($_POST["catedit"])) {
    require_once "dbc.inc.php";
    require_once __DIR__ . '/../bootstrap.php';

    // Begin sql with the table name and operation
    $sql = "UPDATE menucategory SET ";
    $stmt = mysqli_stmt_init($conn);

    /*
     * Write sql code piece by piece depending on what the user inputted
     *
     * Binding was deemed not necessary as values are passed by the the html page and this page is only accessible
     * to the admins.
     *
     * Also I do not know how to make use of the function and fill it dynamically
     */

    $image_path = "";
    if ($_FILES['image']['tmp_name']) {
        $uploadfile = __DIR__ . "/../img/categories/" . basename($_FILES['image']['name']); // maybe make this a function or an include file

        // If a file with the same name already exists delete it and re upload
        if (file_exists($uploadfile)) {
            unlink($uploadfile);
        }

        // If uploading fails return user back to admin
        if (!move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
            $error = "Download Error Occured!\n";
            echo $twig->render(
                '500.html',
                ['error' => $error]
            );
            exit();
        }

        $uploadfile = "/img/categories/" . basename($_FILES['image']['name']);
        // Add image edit
        $sql .= "categoryImagePath = \"" . $uploadfile . "\" ";
    }

    // Add description change
    if ($description = $_POST['description']) {
        if ($sql !== "UPDATE menucategory SET ") $sql .= ", ";
        $sql .= "categoryDescription='" . $description . "' ";
    }

    // Add name change
    if ($name = $_POST['name']) {
        if ($sql !== "UPDATE menucategory SET ") $sql .= ", ";
        $sql .= "categoryName='" . $name . "' ";
    }

    // Finalise sql
    $sql .= "WHERE itemCategoryID =" . $_POST['categoryid'] . ";";

    /*
     * Use mysqli_stmt_prepare() to prepare the sql
     * If the return is: False return user to the previous page and display an error
     *                   True continue
     */

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        echo $twig->render('500.html');
        exit();
    } else {
        // Execute the sql code return to admin
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        mysqli_close($conn);
        header("Location: ../admin.php?edit=success");
    }
} else {
    header("Location: ../index.php");
    exit();
}
